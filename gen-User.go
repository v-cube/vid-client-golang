package vid

// This is file is auto-generated.  Do not modify.  See ./gen/VIDClient-gen.go for more clues

import (
	"encoding/xml"
	// "fmt"
)

// This is an api method
func (v *VIDClient) CheckVcubeId(vcubeId string) (*VIDCheckVcubeIdResponse, error) {
	params := map[string]interface{}{
		"vcubeId":     vcubeId,
		"consumerKey": v.config.ConsumerKey,
	}

	resp, err := v.fRest("/exist/user", "GET", params)

	vid_response := &VIDCheckVcubeIdResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

// This is an api method
func (v *VIDClient) GetContract(vcubeId string) (*VIDGetContractResponse, error) {
	params := map[string]interface{}{
		"vcubeId":        vcubeId,
		"consumerKey":    v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}

	resp, err := v.fRest("/user/contract", "GET", params)

	vid_response := &VIDGetContractResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

// This is an api method
func (v *VIDClient) AddVcubeId(agencyId int, vcubeId, vcubePw string, sha1 int, name, email, lang string) (*VIDAddUserResponse, error) {
	params := map[string]interface{}{
		"agencyId":       agencyId,
		"vcubeId":        vcubeId,
		"vcubePw":        vcubePw,
		"sha1":           sha1,
		"name":           name,
		"kana":           "",
		"email":          email,
		"lang":           lang,
		"type":           "regist",
		"consumerKey":    v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}

	resp, err := v.fRest("/user", "POST", params)

	vid_response := &VIDAddUserResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

// This is an api method
func (v *VIDClient) RegisteredService(vcubeId string) (*VIDRegisteredServiceResponse, error) {
	params := map[string]interface{}{
		"vcubeId":        vcubeId,
		"contractId":     0,
		"consumerKey":    v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}

	resp, err := v.fRest("/user/consumer", "GET", params)

	vid_response := &VIDRegisteredServiceResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

// This is an api method
func (v *VIDClient) SetPasswordForSuperAdmin(vcubeId, password string, sha1 int) (*VIDSetPasswordForSuperAdminResponse, error) {
	params := map[string]interface{}{
		"vcubeId":     vcubeId,
		"vcubePwNew":  password,
		"sha1":        sha1,
		"consumerKey": v.config.ConsumerKey,
	}

	resp, err := v.fRest("/user/password", "GET", params)

	vid_response := &VIDSetPasswordForSuperAdminResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

// This is an api method
func (v *VIDClient) RemoveService(vcubeId string, contractId int) (*VIDRemoveServiceResponse, error) {
	params := map[string]interface{}{
		"vcubeId":        vcubeId,
		"contractId":     contractId,
		"consumerKey":    v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}

	resp, err := v.fRest("/user/consumer", "DELETE", params)

	vid_response := &VIDRemoveServiceResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}
