package vid

// This is file is auto-generated.  Do not modify.  See ./gen/VIDClient-gen.go for more clues

import (
	"encoding/xml"
	// "fmt"
)

// This is an api method
func (v *VIDClient) RequestToken(vcubeId, sha1_password string) (*VIDRequestTokenResponse, error) {
	params := map[string]interface{}{
		"vcubeId":     vcubeId,
		"vcubePw":     sha1_password,
		"consumerKey": v.config.ConsumerKey,
	}

	resp, err := v.fRest("/token", "POST", params)

	vid_response := &VIDRequestTokenResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

// This is an api method
func (v *VIDClient) CheckToken(authToken string) (*VIDCheckTokenResponse, error) {
	params := map[string]interface{}{
		"vIdAuthToken": authToken,
		"consumerKey":  v.config.ConsumerKey,
	}

	resp, err := v.fRest("/exist/token", "GET", params)

	vid_response := &VIDCheckTokenResponse{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}
