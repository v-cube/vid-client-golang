// +build ignore
package main

//go:generate go run VIDClient-gen.go

import (
	"fmt"
	"log"
	"os"
)

var commands = []Command{
	{file: "Token", method: "RequestToken", params_sig: "vcubeId, sha1_password string", params: `map[string]interface{}{
		"vcubeId": vcubeId,
		"vcubePw": sha1_password,
		"consumerKey": v.config.ConsumerKey,
	}`, returnType: "VIDRequestTokenResponse", restPath: "/token", httpMethod: "POST"},
	{file: "Token", method: "CheckToken", params_sig: "authToken string", params: `map[string]interface{}{
		"vIdAuthToken": authToken,
		"consumerKey": v.config.ConsumerKey,
	}`, returnType: "VIDCheckTokenResponse", restPath: "/exist/token", httpMethod: "GET"},
	{file: "User", method: "CheckVcubeId", params_sig: "vcubeId string", params: `map[string]interface{}{
		"vcubeId": vcubeId,
		"consumerKey": v.config.ConsumerKey,
	}`, returnType: "VIDCheckVcubeIdResponse", restPath: "/exist/user", httpMethod: "GET"},
	{file: "User", method: "GetContract", params_sig: "vcubeId string", params: `map[string]interface{}{
		"vcubeId": vcubeId,
		"consumerKey": v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}`, returnType: "VIDGetContractResponse", restPath: "/user/contract", httpMethod: "GET"},
	{file: "User", method: "AddVcubeId", params_sig: "agencyId int, vcubeId, vcubePw string, sha1 int, name, email, lang string", params: `map[string]interface{}{
		"agencyId": agencyId,
		"vcubeId": vcubeId,
		"vcubePw": vcubePw,
		"sha1": sha1,
		"name": name,
		"kana": "",
		"email": email,
		"lang": lang,
		"type": "regist",
		"consumerKey": v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}`, returnType: "VIDAddUserResponse", restPath: "/user", httpMethod: "POST"},
	{file: "User", method: "RegisteredService", params_sig: "vcubeId string", params: `map[string]interface{}{
		"vcubeId": vcubeId,
		"contractId": 0,
		"consumerKey": v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}`, returnType: "VIDRegisteredServiceResponse", restPath: "/user/consumer", httpMethod: "GET"},
	{file: "User", method: "SetPasswordForSuperAdmin", params_sig: "vcubeId, password string, sha1 int", params: `map[string]interface{}{
		"vcubeId": vcubeId,
		"vcubePwNew": password,
		"sha1": sha1,
		"consumerKey": v.config.ConsumerKey,
	}`, returnType: "VIDSetPasswordForSuperAdminResponse", restPath: "/user/password", httpMethod: "GET"},
	{file: "User", method: "RemoveService", params_sig: "vcubeId string, contractId int", params: `map[string]interface{}{
		"vcubeId": vcubeId,
		"contractId": contractId,
		"consumerKey": v.config.ConsumerKey,
		"secretAuthCode": v.config.SecretAuthCode,
	}`, returnType: "VIDRemoveServiceResponse", restPath: "/user/consumer", httpMethod: "DELETE"},
}

type Command struct {
	file, method, params_sig, params, returnType, restPath, httpMethod string
}

func AppendFile(filename, data string) {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	defer file.Close()

	len, err := file.WriteString(data)
	if err != nil {
		log.Fatalf("failed writing to file: %s", err)
	}
	fmt.Printf("\nLength: %d bytes", len)
	fmt.Printf("\nFile Name: %s\n", file.Name())
}

func main() {
	method_counts := make(map[string]int)

	for _, command := range commands {
		file := "../gen-" + command.file + ".go"

		_, err := os.Stat(file)

		if err == nil {
			os.Remove(file)
		}
	}

	for _, command := range commands {
		file := "../gen-" + command.file + ".go"

		_, err := os.Stat(file)

		if err != nil {

			fmt.Println("err", err)
		}

		package_declaration := ""

		if method_counts[command.file] == 0 {
			package_declaration = `
package vid

// This is file is auto-generated.  Do not modify.  See ./gen/VIDClient-gen.go for more clues

import (
	"encoding/xml"
	// "fmt"
)
`
		}

		data := fmt.Sprintf(`
%s

// This is an api method
func (v *VIDClient) %s(%s) (*%s, error) {
	params := %s

	resp,err := v.fRest("%s", "%s", params)

	vid_response := &%s{}

	err = xml.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}
		`, package_declaration, command.method, command.params_sig, command.returnType, command.params, command.restPath, command.httpMethod, command.returnType)

		AppendFile("../gen-"+command.file+".go", data)

		fmt.Println("method_counts[command.file]", method_counts[command.file])

		method_counts[command.file] = method_counts[command.file] + 1
	}
}
