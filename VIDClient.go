package vid

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"github.com/go-resty/resty/v2"
)

type VIDClient struct {
	config *VIDClientConfig
}

type VIDClientConfig struct {
	ConsumerKey, RestPassword, BaseUrl, SecretAuthCode string
	NonceFn                                            func() string
}

type RestDetail struct {
	username,
	password,
	nonce,
	nonce_64,
	created, passwordDigest string
}

// Bla bla
type VIDResponse struct {
	Code   int  `xml:"code"`
	Status int  `xml:"status"`
	Result bool `xml:"result"`
	Error  struct {
		Message string `xml:"message"`
	} `xml:"error"`
}

// bla
type VIDCheckVcubeIdResponse struct {
	VIDResponse
	IsDelete bool `xml:"isDelete"`
}

type VIDAddUserResponse struct {
	VIDResponse
}

type VIDRemoveServiceResponse struct {
	VIDResponse
}

type VIDRegisteredServiceResponse struct {
	VIDResponse
}

type VIDSetPasswordForSuperAdminResponse struct {
	VIDResponse
}

type VIDCheckTokenResponse struct {
	VIDResponse
	VcubeId string `xml:"vcubeId"`
}

type VIDGetContractResponse struct {
	VIDResponse
	ConsumerId string `xml:"consumerId"`
	Contracts  []struct {
		Contract struct {
			ContractId int `xml:"contractId"`
		} `xml:"contract"`
	} `xml:"contracts"`
}

type VIDRequestTokenResponse struct {
	VIDResponse
	VIdAuthToken       string `xml:"vIdAuthToken"`
	ConsumerUserUserId string `xml:"consumeruserUserId"`
	PurgeTime          int    `xml:"purgeTime"`
}

type restFunc func(string) (*resty.Response, error)

var DefaultNonceFn = func() string {
	return strconv.Itoa(int(time.Now().UnixNano() / 1000000))
}

func (v *VIDClient) fRest(rest_path, method string, params map[string]interface{}) (*resty.Response, error) {
	// fmt.Println("------ VIDClient.go:: func (v *VIDClient) fRest(rest_path, method string, params map[string]string) (*VIDResponse, error) {")

	_params := ""

	for key, value := range params {
		_value := fmt.Sprintf("%v", value)

		_params = _params + key + "=" + url.QueryEscape(_value) + "&"
	}

	fmt.Println("_params:", _params)
	fmt.Println("params:", params)

	config := v.config

	restDetail := &RestDetail{
		created:  fmt.Sprint(time.Now().Format(time.RFC3339)),
		password: config.RestPassword,
		username: config.ConsumerKey,
	}

	if config.NonceFn == nil {
		restDetail.nonce = DefaultNonceFn()
	} else {
		restDetail.nonce = config.NonceFn()
	}

	restDetail.nonce = fmt.Sprintf("%x", sha1.Sum([]byte(restDetail.nonce)))
	restDetail.nonce_64 = base64.StdEncoding.EncodeToString([]byte((restDetail.nonce)))

	restDetail.passwordDigest = base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%x", sha1.Sum([]byte(restDetail.nonce+restDetail.created+restDetail.password)))))

	fmt.Println("restDetail:", *restDetail)

	headers := map[string]string{
		"Content-Type":  "application/json",
		"X-WSSE":        fmt.Sprintf("UsernameToken Username=\"%s\", PasswordDigest=\"%s\", Nonce=\"%s\", Created=\"%s\"", restDetail.username, restDetail.passwordDigest, restDetail.nonce_64, restDetail.created),
		"Cache-Control": "no-cache",
	}

	// Create a Resty Client
	client := resty.New()

	var fn restFunc

	// test
	// fmt.Println("1111 fn:", fn)

	request := client.R().SetHeaders(headers)

	fmt.Println("request:", request)

	switch method {
	case "GET":
		fn = request.SetQueryString(_params).EnableTrace().Get
	case "POST":
		fn = request.SetQueryString(_params).SetHeader("Content-Type", "text/plain").EnableTrace().Post
	}

	// fmt.Println("2222 fn:", fn)
	fmt.Println("2222 request:", request)

	resp, err := fn(v.config.BaseUrl + rest_path)

	// fmt.Println("resp.RawBody():", resp.String())

	// Explore response object
	// fmt.Println("Response Info:")
	// fmt.Println("Error      :", err)
	// fmt.Println("Header		:", resp.Header())
	// fmt.Println("Status Code:", resp.StatusCode())
	// fmt.Println("Status     :", resp.Status())
	// fmt.Println("Time       :", resp.Time())
	// fmt.Println("Received At:", resp.ReceivedAt())
	// fmt.Println("Body       :\n", resp)
	// fmt.Println()

	// Explore trace info
	// fmt.Println("Request Trace Info:")
	// ti := resp.Request.TraceInfo()
	// fmt.Println("DNSLookup    :", ti.DNSLookup)
	// fmt.Println("ConnTime     :", ti.ConnTime)
	// fmt.Println("TLSHandshake :", ti.TLSHandshake)
	// fmt.Println("ServerTime   :", ti.ServerTime)
	// fmt.Println("ResponseTime :", ti.ResponseTime)
	// fmt.Println("TotalTime    :", ti.TotalTime)
	// fmt.Println("IsConnReused :", ti.IsConnReused)
	// fmt.Println("IsConnWasIdle:", ti.IsConnWasIdle)
	// fmt.Println("ConnIdleTime :", ti.ConnIdleTime)

	if err != nil {
		return nil, err
	}

	return resp, err
}

// Create a new VIDClient instance
func NewVIDClient(config *VIDClientConfig) *VIDClient {
	return &VIDClient{
		config: config,
	}
}
