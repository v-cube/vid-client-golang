package main

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	vid "bitbucket.org/v-cube/vid-client-golang"
)

func main() {
	jsonFile, err := os.Open("../config_dev.json")
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var dev_config = make(map[string]string)

	json.Unmarshal(byteValue, &dev_config)

	fmt.Println("dev_config:", dev_config)

	defer jsonFile.Close()

	config := &vid.VIDClientConfig{
		ConsumerKey:    dev_config["consumer_key"],
		RestPassword:   dev_config["rest_password"],
		BaseUrl:        dev_config["rest_url"],
		SecretAuthCode: dev_config["secret_auth_code"],
	}

	vid_user1 := dev_config["vid_user1"]
	vid_user1_password := dev_config["vid_user1_password"]

	vid_user2 := dev_config["vid_user2"]

	client := vid.NewVIDClient(config)

	fmt.Println("vid_user1:", vid_user1)

	checkvid_resp, err := client.CheckVcubeId(vid_user1)

	if err != nil {
		panic(err)
	}

	if checkvid_resp.Result && checkvid_resp.Status == 1 {
		fmt.Printf("User %s exists\n", vid_user1)

		requestToken_resp, err := client.RequestToken(vid_user1, fmt.Sprintf("%x", sha1.Sum([]byte(vid_user1_password))))

		if err != nil {
			panic(err)
		}

		fmt.Println("requestToken_resp:", requestToken_resp)
	}

	checkvid_resp, err = client.CheckVcubeId(vid_user2)

	if err != nil {
		panic(err)
	}

	if checkvid_resp.Result && checkvid_resp.Status == 1 {
		fmt.Printf("User %s exists\n", vid_user2)
	}

	fmt.Println("checkvid_resp:", checkvid_resp)
	fmt.Println("checkvid_resp.IsDelete:", checkvid_resp.IsDelete)
	fmt.Println("checkvid_resp.Status:", checkvid_resp.Status)
	fmt.Println("checkvid_resp.Code:", checkvid_resp.Code)

	adduser_resp, err := client.AddVcubeId(1, vid_user2, fmt.Sprintf("%x", sha1.Sum([]byte(vid_user1_password))), 1, "Harold", vid_user2, "en")

	if err != nil {
		panic(err)
	}

	fmt.Println("adduser_resp:", adduser_resp)

	if adduser_resp.Result && adduser_resp.Code == 1000 {
		fmt.Printf("User %s successfully created.\n", vid_user2)
	} else {
		fmt.Printf("User failed to create user %v (code: %v).\n", vid_user2, adduser_resp.Code)
	}

}
