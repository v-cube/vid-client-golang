package vid

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestNewClient(t *testing.T) {
	jsonFile, err := os.Open("config_dev.json")
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var dev_config = make(map[string]string)

	json.Unmarshal(byteValue, &dev_config)

	fmt.Println("dev_config:", dev_config)

	defer jsonFile.Close()

	config := &VIDClientConfig{
		ConsumerKey:    dev_config["consumer_key"],
		RestPassword:   dev_config["rest_password"],
		BaseUrl:        dev_config["rest_url"],
		SecretAuthCode: dev_config["secret_auth_code"],
	}

	vid_user1 := dev_config["vid_user1"]
	vid_user1_password := dev_config["vid_user1_password"]

	client := NewVIDClient(config)

	fmt.Println("client:", client)

	checkvcubeid_resp, err := client.CheckVcubeId(vid_user1)

	if err != nil {
		panic(err)
	}

	_json, err := (json.Marshal(&checkvcubeid_resp))

	fmt.Println("checkvcubeid_resp:", string(_json))

	request_token_resp, err := client.RequestToken(vid_user1, fmt.Sprintf("%x", sha1.Sum([]byte(vid_user1_password))))

	if err != nil {
		panic(err)
	}

	fmt.Println("request_token_resp:", request_token_resp)
	fmt.Println("request_token_resp.ConsumerUserUserId:", request_token_resp.ConsumerUserUserId)

	get_contract_resp, err := client.GetContract(vid_user1)

	if err != nil {
		panic(err)
	}

	_json, err = (json.Marshal(&get_contract_resp))

	fmt.Println("get_contract_resp:", string(_json))

	// fmt.Println("get_contract_resp.ConsumerUserUserId:", get_contract_resp.ConsumerUserUserId)

	check_token_resp, err := client.CheckToken(request_token_resp.VIdAuthToken)

	if err != nil {
		panic(err)
	}

	_json, err = (json.Marshal(&check_token_resp))

	fmt.Println("check_token_resp:", string(_json))
	// fmt.Println("check_token_resp.ConsumerUserUserId:", check_token_resp.ConsumerUserUserId)
}
